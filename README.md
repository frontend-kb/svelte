[[_TOC_]]

## About Svelte
See [Svelte vs React](https://massivepixel.io/blog/svelte-vs-react/)

**tl;dr**

![svelte and react comparison table](img/svelte-vs-react.jpg "Svelte vs React")

in addition to the above:

### Why?

- Svelte *uses the web* (like Remix comparing to Next)

### Why not?

- not backed by a huge company (and its funds), so 
